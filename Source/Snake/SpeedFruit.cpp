// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedFruit.h"
#include <Snake\SnakeBase.h>


// Sets default values
ASpeedFruit::ASpeedFruit()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedFruit::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ASpeedFruit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedFruit::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			
			Snake->SetActorTickInterval(0.1);
			

		}
	}
	
	
}

